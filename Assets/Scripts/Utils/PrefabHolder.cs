﻿using UnityEngine;
using System.Collections;

public class PrefabHolder : MonoBehaviour {

    public static PrefabHolder instance;

    [HideInInspector]
    public PoolManager pool;

    public GameObject redBulletPrefab;
    public GameObject blueBulletPrefab;
    public GameObject crimsonBulletPrefab;

    public GameObject hugeRock;
    public GameObject bigRock;
    public GameObject smallRock;
    public GameObject tinyRock;

    public GameObject spark;
    public GameObject explosion; 

    public GameObject health;
    public GameObject rateOfFire;
    public GameObject timeShift;

    void Awake () {
        pool = PoolManager.instance;
        instance = this;

        pool.CreatePool(redBulletPrefab, 100);
        pool.CreatePool(blueBulletPrefab, 100);
        pool.CreatePool(crimsonBulletPrefab, 100);

        pool.CreatePool(hugeRock, 100);
        pool.CreatePool(bigRock, 100);
        pool.CreatePool(smallRock, 100);
        pool.CreatePool(tinyRock, 100);

        pool.CreatePool(spark, 1000);
        pool.CreatePool(explosion, 100);

        pool.CreatePool(health, 10);
        pool.CreatePool(timeShift, 10);
        pool.CreatePool(rateOfFire, 10);
    }
	
	public void ReuseObject(GameObject prefab, Vector3 position, Quaternion rotation)
    {
        pool.ReuseObject(prefab, position, rotation);
    }
}
