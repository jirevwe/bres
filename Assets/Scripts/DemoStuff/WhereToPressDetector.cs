﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class WhereToPressDetector : MonoBehaviour {

    public Image counter;
    public GameObject parent;

    private void OnEnable()
    {
        counter.fillAmount = 0;
    }

    private void Update()
    {
//#if UNITY_EDITOR
//        if (Input.GetMouseButton(0))
//        {
//            Vector3 _positionToMoveTo = Camera.main.ScreenToWorldPoint(Input.mousePosition);
//            _positionToMoveTo.z = gameObject.transform.position.z;

//            var col = Physics2D.OverlapCircle(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10)), 1f, parent.GetComponent<MoveHelper>().movePosLayer);
//            if (col != null)
//            {
//                DOTween.To(() => counter.fillAmount, size => counter.fillAmount = size, 1, 3f).OnComplete(() =>
//                {
//                    parent.GetComponent<MoveHelper>().CallNext();
//                    counter.fillAmount = 0;
//                });
//            }
//        }
//        else
//        {
//            counter.fillAmount = 0;
//            DOTween.Clear();
//        }
//#endif
        if (Input.touchCount < 2)
        {
            counter.fillAmount = 0;
            DOTween.Clear();
        }
        else if (Input.touchCount == 2)
        {
            Vector3 _positionToMoveTo = Camera.main.ScreenToWorldPoint(Input.GetTouch(1).position);
            _positionToMoveTo.z = gameObject.transform.position.z;

            var col = Physics2D.OverlapCircle(Camera.main.ScreenToWorldPoint(new Vector3(Input.GetTouch(1).position.x, Input.GetTouch(1).position.y, 10)), 1f, parent.GetComponent<MoveHelper>().movePosLayer);
            if (col != null)
            {
                DOTween.To(() => counter.fillAmount, size => counter.fillAmount = size, 1, 1f).OnComplete(() =>
                {
                    parent.GetComponent<MoveHelper>().CallNext();
                    counter.fillAmount = 0;
                });
            }
            else
            {
                counter.fillAmount = 0;
                DOTween.Clear();
            }
        }
         
    }

    private void OnDisable()
    {
        counter.fillAmount = 0;
        DOTween.Clear();
    }
}
