﻿using UnityEngine;

public class RateOfFirePickUp : PoolObject {

    public override void OnObjectReuse()
    {
        base.OnObjectReuse();
    }

    void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player") {
            GameController.Instance.PlaySound(GameController.Instance.click);
            col.gameObject.SendMessage("IncRateOfFire", Random.Range(1, 4));
			gameObject.SetActive(false);
		}
	}

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            GameController.Instance.PlaySound(GameController.Instance.click);
            col.gameObject.SendMessage("IncRateOfFire", Random.Range(1, 4));
            gameObject.SetActive(false);
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            GameController.Instance.PlaySound(GameController.Instance.click);
            col.gameObject.SendMessage("IncRateOfFire", Random.Range(1, 4));
            gameObject.SetActive(false);
        }
    }
}
