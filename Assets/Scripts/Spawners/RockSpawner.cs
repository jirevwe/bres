﻿using UnityEngine;

public class RockSpawner : MonoBehaviour {

    public float spawnTime = 0.3f;
    private float counter = 0f;

    public GameObject[] ObjToSpawn;

    private GameObject crossHair;
    private GameObject rockHolder;
    public GameObject player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

	void OnEnable()
	{
		spawnTime = 7f;
	}

    void Spawn()
    {
        //GameController.GC.grid[Random.Range(0, (int)GameController.GC.grid.gridWorldSize.x), Random.Range(0, (int)GameController.GC.grid.gridWorldSize.y)].worldPosition,
        PrefabHolder.instance.pool.ReuseObject(ObjToSpawn[Random.Range(0, ObjToSpawn.Length)],
            gameObject.transform.root.position,
            Quaternion.Euler(0, 0, Mathf.Atan2(transform.position.y - player.transform.root.position.y, transform.position.x - player.transform.root.position.x) * Mathf.Rad2Deg + 90));
    }

    void Update()
    {
        if (counter > spawnTime && GameController.Instance.state == GameController.GameState.Playing)
        {
            Spawn();
            counter = 0;
        }        
        counter += Time.deltaTime;
		spawnTime -= ((Time.deltaTime / Time.timeScale) / 100);
    }
}
