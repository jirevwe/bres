﻿using DG.Tweening;
using UnityEngine;

public class MoveHelper : MonoBehaviour {

    public bool isClicked = false;
    public LayerMask touchPosLayer, movePosLayer;
    public TextMesh myText;
    public GameObject whereToPress;

	void Start () {
		myText.text = "Touch me";
	}

    public void CallNext()
    {
        if (DemoController.demo.currentItem < DemoController.demo.demoStuffs.Count - 1)
            DemoController.demo.demoStuffs[++DemoController.demo.currentItem].SetActive(true);

        gameObject.SetActive(false);
    }
	
	void Update () {
#if UNITY_EDITOR
        if (Input.GetMouseButton(0))
        {
            Vector3 _positionToMoveTo = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            _positionToMoveTo.z = gameObject.transform.position.z;

            var col = Physics2D.OverlapCircle(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10)), 1f, touchPosLayer);
            if (col != null)
            {
                myText.text = "Touch me";
                DOTween.To(() => myText.fontSize, size => myText.fontSize = size, 18, .5f);
                myText.gameObject.transform.DOLocalMoveY(.3f, .5f);
                isClicked = true;
            }
            else
            {
                myText.text = "Touch me";
                DOTween.To(() => myText.fontSize, size => myText.fontSize = size, 30, .5f);
                myText.gameObject.transform.DOLocalMoveY(.5f, .5f);
                isClicked = false;
            }
        }
        else
        {
            myText.text = "Touch me";
            DOTween.To(() => myText.fontSize, size => myText.fontSize = size, 30, .5f);
            myText.gameObject.transform.DOLocalMoveY(.5f, .5f);
            isClicked = false;
        }
#endif
        if (Input.touchCount > 0)
        {
            Vector3 _positionToMoveTo = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            _positionToMoveTo.z = gameObject.transform.position.z;

            if (Input.touchCount == 1)
            {
                var col = Physics2D.OverlapCircle(Camera.main.ScreenToWorldPoint(new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, 10)), 1f, touchPosLayer);
                if (col != null)
                {
                    myText.text = "Touch me";
                    DOTween.To(() => myText.fontSize, size => myText.fontSize = size, 18, .5f);
                    myText.gameObject.transform.DOLocalMoveY(.3f, .5f);
                    isClicked = true;
                }
                else
                {
                    myText.text = "Touch me";
                    DOTween.To(() => myText.fontSize, size => myText.fontSize = size, 30, .5f);
                    myText.gameObject.transform.DOLocalMoveY(.5f, .5f);
                    isClicked = false;
                }
            }
        }
        else
        {
            myText.text = "Touch me";
            DOTween.To(() => myText.fontSize, size => myText.fontSize = size, 30, .5f);
            myText.gameObject.transform.DOLocalMoveY(.5f, .5f);
            isClicked = false;
        }

        whereToPress.SetActive(isClicked);
    }
}
