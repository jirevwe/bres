﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SplashScreen : MonoBehaviour {

    Image image;

	void Start () {
        image = GetComponent<Image>();
        StartCoroutine(Swap());
	}
    

	IEnumerator Swap () {
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene("Main");	
	}
}
