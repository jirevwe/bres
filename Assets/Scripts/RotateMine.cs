﻿using UnityEngine;
using System.Collections;

public class RotateMine : MonoBehaviour {

    float start = 0;
    float end = 360;
    [Range(1, 30)]
    public float step = 10;

    // Update is called once per frame
    void LateUpdate()
    {
        var temp = new Vector3(0, 0, transform.rotation.eulerAngles.z + step);
        transform.rotation = Quaternion.Euler(temp);
    }
}
