﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;

public class PostBuildAtlasCacheClear : MonoBehaviour
{

    void Start()
    {
        DeleteAtlasCache(BuildTarget.Android);
    }

    [PostProcessBuild]
    private static void DeleteAtlasCache(BuildTarget target)
    {
        string projectPath = Application.dataPath; //Asset path
        string atlasCachePath = Path.GetFullPath(projectPath + @"\..\Library\AtlasCache");
        if (Directory.Exists(atlasCachePath))
        {
            Directory.Delete(atlasCachePath, true);
            Debug.Log("Deleted atlas cache folder.");
        }
    }
}
#endif

