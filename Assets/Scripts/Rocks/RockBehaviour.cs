﻿using UnityEngine;

public class RockBehaviour : PoolObject {

	private float rockHealth;
	private GameObject _player;

    [SerializeField]
    private float timeUP;
	[SerializeField]
    private GameObject wave;
	[SerializeField]
    private float scoreToPlayer;
	[SerializeField]
    private GameObject pickUpH;
	[SerializeField]
    private GameObject pickUpR;
	[SerializeField]
    private GameObject pickUpT;
	[SerializeField]
    private float damageToPlayer;
    [SerializeField]
    private float actualRockHealth;
    [SerializeField]
    private float rockSpeed = 0.1f;
    [SerializeField]
    private AudioClip explodeSound;
    [SerializeField]
    private GameObject explosionSpark;

    private bool follow;
    private Rigidbody2D me;

    void Awake()
    {
        me = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        Die();
    }

    void FixedUpdate()
    {
        if (follow)
            me.velocity = (_player.transform.position - transform.position).normalized * Time.deltaTime * rockSpeed * rockSpeed;
        else
            me.AddRelativeForce(new Vector2(0, Time.deltaTime * rockSpeed * rockSpeed), ForceMode2D.Force);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.SendMessage("DecreaseHealth", damageToPlayer);
            rockHealth = 0f;
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.SendMessage("DecreaseHealth", damageToPlayer);
            rockHealth = 0f;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.SendMessage("DecreaseHealth", damageToPlayer);
            rockHealth = 0f;
        }
    }

    public override void OnObjectReuse()
    {
        base.OnObjectReuse();
        _player = GameObject.FindWithTag("Player");
        rockHealth = GetActualRockHealth();
        follow = true;
        Invoke("Fade", timeUP);
    }

    private int PlayerHit(int health)
    {
        return -health;
    }

    void Fade()
    {
        follow = false;
    }

    void Explode()
    {
        for (int i = 0;i < 100;i++)
        {
			PrefabHolder.instance.pool.ReuseObject(explosionSpark, transform.position, Quaternion.Euler(0, 0, Random.Range(0, 360)));
        }

		if (wave != null) {
			GameObject g = (GameObject)Instantiate (wave, transform.position, Quaternion.identity);
			Destroy(g, 2f);
		}

        if(gameObject.name.Contains("rock_huge"))
            CameraShake.shakeDuration = 0.1f;

        if (Time.timeScale == 1)
        {
            float f = Random.Range(0, 21);
            if (f == 20)
                PrefabHolder.instance.pool.ReuseObject(pickUpH, transform.position, Quaternion.identity);
            else if (f == 0)
                PrefabHolder.instance.pool.ReuseObject(pickUpR, transform.position, Quaternion.identity);
            else if (f == 10)
                PrefabHolder.instance.pool.ReuseObject(pickUpT, transform.position, Quaternion.identity);
        }
        else
        {
            float f = Random.Range(0, 11);
            if (f == 10)
                PrefabHolder.instance.pool.ReuseObject(pickUpH, transform.position, Quaternion.identity);
            else if (f == 0)
                PrefabHolder.instance.pool.ReuseObject(pickUpR, transform.position, Quaternion.identity);
        }
    }

    public float GetActualRockHealth()
    {
        return actualRockHealth;
    }

    void InitHealth(float DefaultAmt)
    {
        rockHealth = DefaultAmt;
    }
       
    void DecreaseHealth(float amt)
    {
        rockHealth -= amt;
    }
	
    void Die()
    {
		if(rockHealth <= 0f){
			GameController.Instance.SessionScore += scoreToPlayer;
            GameController.Instance.PlaySound(explodeSound);
            Explode();
			InitHealth(GetActualRockHealth());
			gameObject.SetActive(false);
        }
    }
}