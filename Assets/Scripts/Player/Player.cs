﻿using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    private GameObject crossHair;
	public float rateOfFireUpgrade = 1;
    public GameObject wave;
    public float maxHealth;
    public float speed;

	[HideInInspector]
    public int deviation = 0;

	[SerializeField]
    private float rateOfFire = 0.1f;
	[SerializeField]
    private Text HP;
    [SerializeField]
    private float playerHealth;
    private BulletSpawner mainCannon;

	public float Health{
		set{playerHealth = value;}
	}

	public float ROFUpgrade
	{
		get{return rateOfFireUpgrade;}
		set{rateOfFireUpgrade = value;}
	}

	public float RateOfFire
	{
		get{return rateOfFire;}
		set{rateOfFire = value;}
	}

	void Awake () {
        crossHair = GameObject.FindGameObjectWithTag("cross_hair");
	}

	public void IncRateOfFire(int type)
	{
		rateOfFire -= 0.01f;
		rateOfFireUpgrade += 1;
        if (GetComponents<BulletSpawner>().Length == 1) {
            if (type == 1) {
                BulletSpawner b = gameObject.AddComponent<BulletSpawner>();
                b.bullet = PrefabHolder.instance.redBulletPrefab;
                b.Deviation = 1;
                b.RateOfFire = rateOfFire;

                BulletSpawner b2 = gameObject.AddComponent<BulletSpawner>();
                b2.bullet = PrefabHolder.instance.redBulletPrefab;
                b2.Deviation = -1;
                b2.RateOfFire = rateOfFire;

                Destroy(b2, 10f);
                Destroy(b, 10f);
            }
            if (type == 2) {
                BulletSpawner b = gameObject.AddComponent<BulletSpawner>();
                b.bullet = PrefabHolder.instance.crimsonBulletPrefab;
                b.Deviation = 1;
                b.RateOfFire = rateOfFire;

                BulletSpawner b2 = gameObject.AddComponent<BulletSpawner>();
                b2.bullet = PrefabHolder.instance.crimsonBulletPrefab;
                b2.Deviation = -1;
                b2.RateOfFire = rateOfFire;

                Destroy(b2, 10f);
                Destroy(b, 10f);
            }
            if (rateOfFireUpgrade >= 5 && type == 3) {
                BulletSpawner b = gameObject.AddComponent<BulletSpawner>();
                b.bullet = PrefabHolder.instance.redBulletPrefab;
                b.Deviation = 1;
                b.RateOfFire = rateOfFire;

                BulletSpawner b2 = gameObject.AddComponent<BulletSpawner>();
                b2.bullet = PrefabHolder.instance.redBulletPrefab;
                b2.Deviation = -1;
                b2.RateOfFire = rateOfFire;

                BulletSpawner b3 = gameObject.AddComponent<BulletSpawner>();
                b3.bullet = PrefabHolder.instance.crimsonBulletPrefab;
                b3.Deviation = 3;
                b3.RateOfFire = rateOfFire;

                BulletSpawner b4 = gameObject.AddComponent<BulletSpawner>();
                b4.bullet = PrefabHolder.instance.crimsonBulletPrefab;
                b4.Deviation = -3;
                b4.RateOfFire = rateOfFire;

                Destroy(b4, 10f);
                Destroy(b3, 10f);
                Destroy(b2, 10f);
                Destroy(b, 10f);
            } else if (type == 3 && rateOfFireUpgrade < 5) {
                IncRateOfFire(Random.Range(1, 3));
            }
        }
	}

	void IncHealth(float amt)
	{
		if (playerHealth < maxHealth)
			playerHealth += amt;
		if (playerHealth > maxHealth)
			playerHealth = maxHealth;
	}

    void DecreaseHealth(float amt)
    {
        playerHealth -= amt;
    }

	void PlayerDie()
	{
        if (wave != null)
        {
            GameObject g = (GameObject)Instantiate(wave, transform.position, Quaternion.identity);
            Destroy(g, 2f);
        }
        CameraShake.shakeDuration = 0.5f;
		GameController.Instance.state = GameController.GameState.Menu;
	}

	void Update () {
		HP.text = (playerHealth / maxHealth * 100).ToString() + "%";

		if (playerHealth <= 0) {
			for (int i = 0;i < 100;i++)
				PrefabHolder.instance.ReuseObject(PrefabHolder.instance.spark, transform.position, Quaternion.Euler(0, 0, Random.Range(0, 360)));
			Invoke("PlayerDie", .1f);
		}

		if (GameController.Instance.state == GameController.GameState.Playing || GameController.Instance.state == GameController.GameState.Demo) {
			transform.Translate (transform.TransformDirection (Input.GetAxis ("Horizontal"), Input.GetAxis ("Vertical"), 0));
			 
			if (Input.GetMouseButton (0)) {
				crossHair.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 10));
			}
            //transform.Translate(Input.acceleration.x * speed * Time.deltaTime, Input.acceleration.y * speed * Time.deltaTime, 0);

			if (Input.touchCount > 0) {
				Vector3 _positionToMoveTo = Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position);
				_positionToMoveTo.z = gameObject.transform.position.z;

				if (Input.touchCount == 1) {
					crossHair.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (Input.GetTouch (0).position.x, Input.GetTouch (0).position.y, 10));
				}
				if (Input.touchCount > 1) {
					crossHair.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (Input.GetTouch (0).position.x, Input.GetTouch (0).position.y, 10));
                    gameObject.MoveTo(Camera.main.ScreenToWorldPoint (new Vector3 (Input.GetTouch (1).position.x, Input.GetTouch (1).position.y, 10)), 0.1f, 0f, EaseType.linear);
                }
            }
		}
	}
}