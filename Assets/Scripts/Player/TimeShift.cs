﻿using UnityEngine;

public class TimeShift : PoolObject {

    public override void OnObjectReuse()
    {
        base.OnObjectReuse();
    }

    void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player") {
            GameController.Instance.PlaySound(GameController.Instance.click);
            GameController.Instance.TimeShift();
			gameObject.SetActive(false);
		}
	}

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            GameController.Instance.PlaySound(GameController.Instance.click);
            GameController.Instance.TimeShift();
            gameObject.SetActive(false);
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            GameController.Instance.PlaySound(GameController.Instance.click);
            GameController.Instance.TimeShift();
            gameObject.SetActive(false);
        }
    }
}
