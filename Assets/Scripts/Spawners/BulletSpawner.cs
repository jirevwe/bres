﻿using UnityEngine;

public class BulletSpawner : MonoBehaviour {

    public GameObject bullet;
    private float addDamage;
	private float counter = 0f;
	private GameObject crossHair;
	private GameObject bulletHolder;
    private AudioClip bulletSound;
	public float deviation = 0;
	private GameObject Player;
	private GameObject[] bulletPrefab;
    [Range(0.1f, 1f)]
	public float rateOfFire;

	public float RateOfFire
	{
		set{rateOfFire = value;}
	}

	public float Deviation
	{
		set{ deviation = value;}
	}

	void Awake()
	{
		crossHair = GameObject.FindGameObjectWithTag ("cross_hair");
		bulletHolder = GameObject.FindGameObjectWithTag ("bullets");
		//bulletSound = Resources.Load ("sFx/sound_weapon_a") as AudioClip;
		Player = GameObject.FindWithTag ("Player");
		//bulletPrefab = Player.GetComponent<Player> ().bulletPrefab;
	}

    void Spawn()
    {
        PrefabHolder.instance.pool.ReuseObject(bullet,
            gameObject.transform.position,
            Quaternion.Euler(0, 0, Mathf.Atan2(transform.position.y - crossHair.transform.position.y, transform.position.x - crossHair.transform.position.x) * Mathf.Rad2Deg + 90 + deviation));
    }

	void Update()
    {
		if(counter > rateOfFire)
		{
			Spawn();
			counter = 0;
		}
        counter += Time.deltaTime * 10 / Time.timeScale;
        rateOfFire = Player.GetComponent<Player>().RateOfFire;
    }
}