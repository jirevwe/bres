﻿using UnityEngine;

public class EnemyBulletSpawner : MonoBehaviour {

	public GameObject ObjToSpawn;
	private float counter = 0f;
	private GameObject player;
	private GameObject bulletHolder;
	private AudioClip bulletSound;
	[SerializeField]private float deviation = 0;
	public float rateOfFire;

	public GameObject Player
	{
		get {return player;}
		set {player = value;}
	}

	public float RateOfFire
	{
		set{rateOfFire = value;}
	}
	
	public float Deviation
	{
		get {return deviation; }
		set{ deviation = value;}
	}
	
	void Awake()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
		bulletHolder = GameObject.FindGameObjectWithTag ("bullets");
		bulletSound = Resources.Load ("sFx/sound_weapon_a") as AudioClip;
	}
	
	void Spawn()
	{
        PoolManager.instance.ReuseObject(ObjToSpawn,
            gameObject.transform.position,
            Quaternion.Euler(0, 0, Mathf.Atan2(transform.position.y - player.transform.position.y, transform.position.x - player.transform.position.x) * Mathf.Rad2Deg + 90 + deviation));
	}
	
	void Update()
	{
		if(counter > rateOfFire)
		{
			Spawn();
			counter = 0;
		}
		counter += Time.deltaTime * 10;
	}
}
