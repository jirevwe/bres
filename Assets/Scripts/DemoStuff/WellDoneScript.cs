﻿using DG.Tweening;
using System.Collections;
using UnityEngine;

public class WellDoneScript : MonoBehaviour {

	void Awake () {
        StartCoroutine(Go());
	}

    IEnumerator Go()
    {
        yield return new WaitForSeconds(2f);
        transform.DOScale(1f, 1f).OnComplete(() =>
        {
            transform.DOScale(0f, 1.5f).OnComplete(() =>
            {
                if (DemoController.demo.currentItem < DemoController.demo.demoStuffs.Count - 1)
                    DemoController.demo.demoStuffs[++DemoController.demo.currentItem].SetActive(true);
            });
        });
    }
	
}
