﻿using UnityEngine;

public class BulletBehaviour : PoolObject
{
    public GameObject bullet;

    public float speed;
    [SerializeField]
    private float bulletDamage;
    [SerializeField]
    private AudioClip hitSound;
    [SerializeField]
    private AudioClip launchSound;

    float liveTime;

    public override void OnObjectReuse()
    {
        base.OnObjectReuse();
        GameController.Instance.PlaySound(launchSound);
        liveTime = Time.time;
    }

    void Update()
    {
        transform.Translate(new Vector3(0, speed * Time.deltaTime / Time.timeScale, 0));
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Walls")
        {
            gameObject.SetActive(false);
        }
        else if (other.tag.Contains("enemy"))
        {
            GameController.Instance.SessionScore += 10f;
            AudioSource.PlayClipAtPoint(hitSound, transform.position);
            other.gameObject.SendMessage("DecreaseHealth", bulletDamage, SendMessageOptions.DontRequireReceiver);
            gameObject.SetActive(false);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Walls")
        {
            gameObject.SetActive(false);
        }
        else if (other.tag.Contains("enemy"))
        {
            GameController.Instance.SessionScore += 10f;
            AudioSource.PlayClipAtPoint(hitSound, transform.position);
            other.gameObject.SendMessage("DecreaseHealth", bulletDamage);
            gameObject.SetActive(false);
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Walls")
        {
            gameObject.SetActive(false);
        }
        else if (other.tag.Contains("enemy"))
        {
            GameController.Instance.SessionScore += 10f;
            AudioSource.PlayClipAtPoint(hitSound, transform.position);
            other.gameObject.SendMessage("DecreaseHealth", bulletDamage);
            gameObject.SetActive(false);
        }
    }
}