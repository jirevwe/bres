﻿using UnityEngine;
using UnityEngine.UI;

public class MenuActions : MonoBehaviour {

	[SerializeField] private Image soundOff;
	[SerializeField] private Image soundOn;

	[SerializeField] private Button help;
	[SerializeField] private Button back;

	[SerializeField] private GameObject MenuStuff;
    [SerializeField] private GameObject HelpStuff;

    [SerializeField]
    private Text helpText;

    [SerializeField]
    private Image[] otherText;

	public void ToggleHelpOff()
	{
		MenuStuff.SetActive (true);
		HelpStuff.SetActive (false);

		back.gameObject.SetActive (false);
		help.gameObject.SetActive (true);
		AudioSource.PlayClipAtPoint (GameController.Instance.click, transform.position);
	}

	public void ToggleHelpOn()
	{
		MenuStuff.SetActive (false);
		HelpStuff.SetActive (true);

		back.gameObject.SetActive (true);
		help.gameObject.SetActive (false);
		AudioSource.PlayClipAtPoint (GameController.Instance.click, transform.position);
	}

	void Start()
	{
		soundOn.gameObject.SetActive(false);
		soundOff.gameObject.SetActive(true);

		MenuStuff.SetActive (true);
		HelpStuff.SetActive (false);

		back.gameObject.SetActive (false);
		help.gameObject.SetActive (true);
	}

	public void ToggleSound()
	{
		if(AudioListener.volume == 1f)
		{
            AudioListener.volume = 0f;
            soundOn.gameObject.SetActive(true);
			soundOff.gameObject.SetActive(false);
		}
		else
		{
            AudioListener.volume = 1f; 
            soundOn.gameObject.SetActive(false);
			soundOff.gameObject.SetActive(true);
		}
		AudioSource.PlayClipAtPoint (GameController.Instance.click, transform.position);
	}

	public void StartGame()
    { 
		GameController.Instance.StartGame ();
	}
}