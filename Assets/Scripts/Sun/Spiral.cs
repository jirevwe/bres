﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 
/// Basic-Spiral: x(t) = at cos(t), y(t) = at sin(t)
/// Circular:     x(t) = R cos(t), y(t) = R sin(t)
/// 
/// </summary>
public class Spiral : MonoBehaviour {

    public float radius; //R
    public float incrementScale;

    public float targetAngle;
    [SerializeField]
    private float angle; //t
    private Vector3 newPosition;

    public float constant;

	// Use this for initialization
	void Start () {
        newPosition = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () {
        angle = angle >= targetAngle ? angle = 0 : angle + Time.deltaTime * incrementScale;

        #region Circular motion
        newPosition.x = radius * Mathf.Cos(Mathf.Deg2Rad * angle);
        newPosition.y = radius * Mathf.Sin(Mathf.Deg2Rad * angle);
        #endregion

        #region Spiral motion
        //var polar = constant * angle;
        //newPosition.x = polar * Mathf.Cos(Mathf.Deg2Rad * angle);
        //newPosition.y = polar * Mathf.Sin(Mathf.Deg2Rad * angle);
        #endregion

        transform.localPosition = Vector3.MoveTowards(transform.localPosition, newPosition, Time.deltaTime * incrementScale);
    }
}
