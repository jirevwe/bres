using UnityEngine;
using System.Collections;

public class PathGenerator : MonoBehaviour{
	int pointCount = 4;
	float pathLength = 10;
	float pointDeviation =3f;
	Vector3[] path = null;
	Vector3 rootPosition;
	
	void Start(){
		GenerateRandomPath();
		iTween.MoveTo(gameObject,iTween.Hash("path",path,"time",7,"easetype",iTween.EaseType.easeInOutCubic,"looptype",iTween.LoopType.pingPong));		
	}
	
	void OnDrawGizmos(){
		if (path != null) {
			if(path.Length>0){
				iTween.DrawPath(path);	
			}	
		}	
	}
	
	void GenerateRandomPath(){
		rootPosition = transform.position;
		path = new Vector3[pointCount + 2];
		float pointGap = pathLength / pointCount;
		path[0] = rootPosition;
		path[pointCount + 1] = new Vector3(rootPosition.x, rootPosition.y, rootPosition.z);
		for (int i = 1; i < pointCount + 1; i++) {
            float randomX = 0;
            float randomY = 0;
            if (rootPosition.x >= 0)
            {
                randomX = rootPosition.x + Random.Range(-pointDeviation, pointDeviation);
                randomY = rootPosition.y + Random.Range(-pointDeviation, pointDeviation);
            }
            if (rootPosition.x < 0)
            {
                randomX = rootPosition.x + Random.Range(-pointDeviation, pointDeviation);
                randomY = rootPosition.y + Random.Range(-pointDeviation, pointDeviation);
            }
            float newZ = rootPosition.z;//(pointGap * i);
			path[i]=new Vector3(randomX,randomY,newZ);
		}
	}
}

