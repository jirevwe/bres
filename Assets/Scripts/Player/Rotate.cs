﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {

	private GameObject player;

	void Awake()
	{
		player = GameObject.FindWithTag ("cross_hair");
	}

	void LateUpdate()
	{
        var newRot = new Vector3(0, 0, Mathf.Atan2(transform.position.y - player.transform.position.y, transform.position.x - player.transform.position.x) * Mathf.Rad2Deg + 90);//, .001f, 0f, EaseType.linear);
        gameObject.transform.rotation = Quaternion.Euler(newRot);        
	}
}
