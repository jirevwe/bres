﻿using DG.Tweening;
using UnityEngine;

public class DemoItem : MonoBehaviour {

    public GameObject explosion, explosion2;
    public AudioClip explode;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "bullet")
        {
            gameObject.GetComponent<CircleCollider2D>().enabled = false;
            
            Explode();

            transform.DOScale(0f, 1f).OnComplete(() => {
                if (DemoController.demo.currentItem < DemoController.demo.demoStuffs.Count - 1)
                    DemoController.demo.demoStuffs[++DemoController.demo.currentItem].SetActive(true);
                gameObject.SetActive(false);
            });
        }
    }

    void Explode()
    {
        PrefabHolder.instance.pool.ReuseObject(explosion2, transform.position, Quaternion.identity);
        GameController.Instance.PlaySound(explode);

        CameraShake.shakeDuration = 0.1f;
    }
}
