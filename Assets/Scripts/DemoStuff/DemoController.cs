﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DemoController : MonoBehaviour {

    public static DemoController demo;
    public int currentItem = 0;

    public List<GameObject> demoStuffs = new List<GameObject>();

	// Use this for initialization
	void Awake () {
        demo = this;
        StartCoroutine(Ikuzo());
	}

    IEnumerator Ikuzo()
    {
        yield return new WaitForSeconds(2f);
        demoStuffs[currentItem].SetActive(true);
    }

    public void GoToNext()
    {
        if (demo.currentItem < demo.demoStuffs.Count - 1)
            demo.demoStuffs[++demo.currentItem].SetActive(true);
        demo.demoStuffs[demo.currentItem - 1].SetActive(false);
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene(0);
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            BackToMenu();
        }
    }
}
