﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public static GameController Instance;
	public float timeScale = 0.1f;
	public float sessionScore = 0f;
	public float Wave = 1;

	public GameObject[] waveGroups1;
	public GameObject[] waveGroups2;
	public GameObject[] waveGroups3;

    private GameObject[] spawns;

 	public GameObject Player = null;
    public AudioClip click;
    public AudioSource audioPlayer;
    public Grid grid;
    public bool isDemo = false;
    
    [SerializeField]
    private Text curScore;
	[SerializeField]
    private Text highScore;
    [SerializeField]
    private Text debugText;

    public Canvas MenuCanvas;
	public Canvas InGameCanvas;

    public GameState state;
	public enum GameState
	{
		Menu,
		Playing,
        Demo
	};

    public GameObject smallRock;
    public List<ObjectInstance> smallRocks;

	[SerializeField]
    private GameObject[] objs;
	[SerializeField]
    private Text scoreText;
	
	public float SessionScore
	{
		set{sessionScore = value;}
		get{return sessionScore;}
	}

	void Start()
	{
        if (!isDemo)
        {
            state = GameState.Menu;
        }
        else
        {
            StartDemo(); state = GameState.Demo;
        }
            
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
	}

	void Awake ()
    {
        Instance = this;
        Application.targetFrameRate = 30;
        spawns = GameObject.FindGameObjectsWithTag("rockSpawn");
        grid = GetComponent<Grid>();
    }

	public void EndGame()
	{
		if(SessionScore > PlayerPrefs.GetFloat("highScore"))
		{
			PlayerPrefs.SetFloat("highScore", SessionScore);
		}

		GameObject[] rof = GameObject.FindGameObjectsWithTag ("rate_of_fire");
		foreach (GameObject o in rof)
			o.SetActive (false);

		GameObject[] healths = GameObject.FindGameObjectsWithTag ("health");
		foreach (GameObject o in healths)
			o.SetActive (false);

		foreach (GameObject o in objs)
			o.SetActive (false);
	}

	public void TimeShift()
	{
		Time.timeScale = 0.3f;
		StartCoroutine (Revert ());
	}

	IEnumerator Revert()
	{
		yield return new WaitForSeconds (5f);
		Time.timeScale = 1f;
	}

	public void StartGame()
	{
        foreach (GameObject g in spawns)
            g.SetActive(true);

        Wave = 3;
        SessionScore = 0f;

		GameObject[] rocks = GameObject.FindGameObjectsWithTag ("enemy_rock");

		foreach (GameObject o in rocks)
			o.SetActive (false);
		objs[0].SetActive (true);

        Player = objs[0];
		Player.GetComponent<Player> ().RateOfFire = 1f;
		Player.GetComponent<Player> ().rateOfFireUpgrade = 0f;
		Player.GetComponent<Player> ().Health = Player.GetComponent<Player>().maxHealth;
        Player.transform.position = Vector3.zero;

        PlaySound(click);
		Instance.state = GameState.Playing;
	}

    void StartDemo()
    {
        PlaySound(click);

        Player = objs[0];
        Player.GetComponent<Player>().RateOfFire = 1f;
        Player.GetComponent<Player>().rateOfFireUpgrade = 0f;
        Player.GetComponent<Player>().Health = Player.GetComponent<Player>().maxHealth;
        Player.transform.position = Vector3.zero;
    }

	void Update () {
        //smallRocks = PoolManager.instance.GetActiveObjects(smallRock);

        scoreText.text = sessionScore.ToString ();

        timeScale = Time.timeScale;

		switch (state) {
		    case GameState.Menu:
                Cursor.visible = true;
			    EndGame();
			    MenuCanvas.gameObject.SetActive(true);
			    InGameCanvas.gameObject.SetActive(false);
			    curScore.text = "SCORE: " + sessionScore.ToString();
			    highScore.text = "HIGHSCORE: " + PlayerPrefs.GetFloat("highScore");
			    if(Input.GetKey(KeyCode.Escape))
				    Application.Quit();
			    break;
		    case GameState.Playing:
                Cursor.visible = false;
                MenuCanvas.gameObject.SetActive(false);
			    InGameCanvas.gameObject.SetActive(true);
			    break;
            case GameState.Demo:
                Cursor.visible = false;
                InGameCanvas.gameObject.SetActive(true);
                break;
        }
	}

    public void PlaySound(AudioClip clip)
    {
        audioPlayer.PlayOneShot(clip, audioPlayer.volume);
    }

	public static void Spawn(GameObject g, Transform t, float timeToLive)
	{
		PoolManager.instance.ReuseObject (g, t.position, Quaternion.identity);
	}

    public void OpenTuts()
    {
        SceneManager.LoadScene(1);
    }
}