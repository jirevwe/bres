﻿using UnityEngine;
using System.Collections;

public class ExplosionSparkBehaviour : PoolObject {

    [SerializeField]
    private float sparkSpeed;

    public override void OnObjectReuse()
    {
        base.OnObjectReuse();
        SetSparkSpeed(Random.Range(0.5f, 1f) * 50f * Time.deltaTime);
    }

    public void SetSparkSpeed(float amt)
    {
        sparkSpeed = amt;
    }
	
	void Update () {
        transform.Translate(new Vector3(0, sparkSpeed / 10, 0));
	}
}
