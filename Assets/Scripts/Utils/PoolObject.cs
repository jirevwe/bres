﻿using UnityEngine;
using System.Collections;

public class PoolObject : MonoBehaviour {

    [SerializeField]
    protected float timeToDie;

    public void DieOH()
    {
        Destroy(timeToDie);
    }

    public virtual void Destroy(float time)
    {
        Invoke("Destroy", time);
    }

	public virtual void OnObjectReuse() {
        DieOH();
    }

	protected void Destroy() {
		gameObject.SetActive (false);
	}
}
